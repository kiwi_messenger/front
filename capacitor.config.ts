import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'tk.epsila',
  appName: 'Epsila',
  webDir: 'dist'
};

export default config;
