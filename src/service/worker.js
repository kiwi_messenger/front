import socket from "@/service/socket.io";
import workerInInterface from "@/service/workerInInterface";
import {
  set,
  setF,
  put,
  get,
  del,
  cleanAll,
  getAll,
  setMultiple,
} from "@/service/database";
import {
  binaryTobase64url,
  base64urlToBinary,
  arraybufferEqual,
  concat,
} from "@/service/function";
import { PubRsa, PriRsa, Aes } from "./crypto";
let rsaKeysList = {};
let aesKeysList = {};
let priRsa;
let aes;
let idAccount;
let id;
let idRequestFriend;

async function traiteMessage({ data, source }) {
  try {
    const idAccountString = binaryTobase64url(source.idAccount);
    const idString = binaryTobase64url(source.id);

    console.log("traiteMessage", { data, source });
    switch (data.type) {
      case "publicMessage":
        workerInInterface.postMessage("message", {
          ...data,
          idChannel: "general",
          idUser: idString,
          name: source.pseudo,
          correspondant: true,
        });
        break;
      case "message": {
        const decriptData = await priRsa.decryptObject(data.data.slice(0, 512));
        console.log(decriptData);
        switch (decriptData.type) {
          case "message":
            workerInInterface.postMessage("messageMP", {
              ...decriptData.data,
              idUser: idString,
              idChannel: idAccountString,
              name: source.pseudo,
              correspondant: true,
            });
            break;
          case "addFriendResponse": {
            if (!idRequestFriend?.includes(idString)) {
              return;
            }
            if (decriptData.success) {
              const dataAes = data.data.slice(512);
              aesKeysList[idAccountString] = await Aes.importRsa(
                dataAes,
                priRsa,
              );
              const friend = {
                key: await Aes.exportRSAToAES(dataAes, priRsa, aes),
                id: source.idAccount,
                pseudo: source.pseudo,
              };
              await set(undefined, friend, "friend");
              workerInInterface.postMessage("addFriend", {
                id: source.idAccount,
                pseudo: source.pseudo,
              });

              const devices = (await getAll("device")).filter(
                (device) => !arraybufferEqual(id, device.id),
              );
              console.log("addFriendResponse", devices);

              data = await aes.encryptObject({
                data: {
                  friend,
                },
                type: "addFriend",
              });
              socket.emit("sendMessage", {
                destination: devices.map((device) => device.id),
                data: { data, type: "internal" },
                persist: true,
              });
            } else {
              // plus tard
            }
            break;
          }
          case "addFriendRequest": {
            idRequestFriend = undefined;
            const idAccountString = binaryTobase64url(source.idAccount);
            workerInInterface.postMessage("addFriendRequest", {
              pseudo: source.pseudo,
            });
            let { success } = await new Promise((resolve) => {
              workerInInterface.addEventListener(
                "addFriendResponse",
                async ({ detail: { success } }) => {
                  resolve({ success });
                },
                { once: true },
              );
            });
            const key = await getRsaKey(source.id);
            if (success) {
              const devices = (await getAll("device")).filter(
                (device) => !arraybufferEqual(id, device.id),
              );
              const {
                crypto,
                exportRsaKeys: exportRsaKeys,
                exportAesKeys,
              } = await Aes.getAes([key], [aes]);

              aesKeysList[idAccountString] = crypto;
              await set(
                undefined,
                {
                  key: exportAesKeys[0],
                  id: source.idAccount,
                  pseudo: source.pseudo,
                },
                "friend",
              );
              workerInInterface.postMessage("addFriend", {
                id: source.idAccount,
                pseudo: source.pseudo,
              });

              const dataEncrypt = await key.encryptObject({
                type: "addFriendResponse",
                success,
              });

              socket.emit("sendMessage", {
                destination: [source.id],
                data: {
                  data: concat(dataEncrypt, exportRsaKeys[0]),
                  type: "message",
                },
                persist: true,
              });
              data = await aes.encryptObject({
                data: {
                  friend: {
                    key: exportAesKeys[0],
                    id: source.idAccount,
                    pseudo: source.pseudo,
                  },
                },
                type: "addFriend",
              });
              socket.emit("sendMessage", {
                destination: devices.map((device) => device.id),
                data: { data, type: "internal" },
                persist: true,
              });
            } else {
              const dataEncrypt = await key.encryptObject({
                type: "addFriendResponse",
                success,
              });

              socket.emit("sendMessage", {
                destination: [source.id],
                data: { data: dataEncrypt, type: "message" },
                persist: false,
              });
            }
            console.log(success);
            break;
          }

          default:
            console.error(data);
        }
        break;
      }
      case "friend": {
        const aes = await getAesKey(source.idAccount);
        const decriptData = await aes.decryptObject(data.data);
        workerInInterface.postMessage("messageMPFriend", {
          ...decriptData.data,
          idUser: idString,
          idChannel: idAccountString,
          name: source.pseudo,
          correspondant: true,
        });
        break;
      }

      case "internal": {
        console.log(data);
        const decriptData = await aes.decryptObject(data.data);
        console.log(decriptData);
        switch (decriptData.type) {
          case "addDevice":
            await set(undefined, decriptData.data.device, "device");
            workerInInterface.postMessage("addDevice", decriptData.data.device);
            break;
          case "putDevice":
            await put(
              decriptData.data.device.id,
              decriptData.data.device,
              "device",
            );
            workerInInterface.postMessage("putDevice", decriptData.data.device);
            break;
          case "delDevice":
            console.log(id, decriptData.data.id);
            if (arraybufferEqual(id, decriptData.data.id)) {
              await cleanAll();
              priRsa = undefined;
              id = undefined;
              idAccount = undefined;
              socket.emit("logout");
              workerInInterface.postMessage("goToCreateAccount");
            } else {
              await del(decriptData.data.id, "device");
              workerInInterface.postMessage("delDevice", decriptData.data);
            }
            break;
          case "addFriend": {
            await set(undefined, decriptData.data.friend, "friend");
            const idAccountFrienString = binaryTobase64url(
              decriptData.data.friend.id,
            );
            workerInInterface.postMessage("addFriend", {
              ...decriptData.data.friend,
            });
            aesKeysList[idAccountFrienString] = await Aes.importAes(
              decriptData.data.friend.key,
              aes,
            );

            break;
          }
          case "messageMP":
            workerInInterface.postMessage("messageMP", {
              ...decriptData.data,
              idUser: idString,
              name: source.pseudo,
              correspondant: false,
            });
            break;
          case "messageMPFriend":
            workerInInterface.postMessage("messageMPFriend", {
              ...decriptData.data,
              idUser: idString,
              name: source.pseudo,
              correspondant: false,
            });
            break;
          case "internalMessage":
            workerInInterface.postMessage("message", {
              ...decriptData.data,
              idUser: idString,
              name: source.pseudo,
              correspondant: true,
            });
            break;
          case "sendDBs":
            workerInInterface.postMessage(
              "setDevices",
              decriptData.data.devices,
            );
            await setMultiple(decriptData.data.devices, "device");
            workerInInterface.postMessage(
              "setFriends",
              decriptData.data.friends,
            );
            await setMultiple(decriptData.data.friends, "friend");
            break;
          default:
            break;
        }
        break;
      }
      default:
        console.error(data);
    }
  } catch (e) {
    console.error(e);
  }
}

async function getRsaKey(id) {
  let idString = id;
  if (typeof id !== "string") {
    idString = binaryTobase64url(id);
  }
  if (!rsaKeysList[idString]) {
    const { code, data: key } = await socket.emitP("getKey", id);
    if (code === 200) {
      rsaKeysList[idString] = await PubRsa.import(key);
    } else {
      throw new Error(`can not getKey error code:${code}`);
    }
  }
  return rsaKeysList[idString];
}

// async function getRsaKeys(pseudo) {
//   const { code, data: keys } = await socket.emitP("getKeys", pseudo);
//   if (code === 200) {
//     return PubRsa.imports(keys);
//   } else {
//     throw new Error(`can not getKey error code:${code}`);
//   }
// }
async function getAesKey(idAccount) {
  let idString = idAccount;
  let idBin = idAccount;

  if (typeof idAccount !== "string") {
    idString = binaryTobase64url(idAccount);
  } else if (!aesKeysList[idAccount]) {
    idBin = base64urlToBinary(idAccount);
  } else {
    return aesKeysList[idString];
  }

  if (!aesKeysList[idString]) {
    const friend = await get(idBin, "friend");
    aesKeysList[idString] = await Aes.importAes(friend.key, aes);
  }
  return aesKeysList[idString];
}

async function login({ pseudo }) {
  const { code, data: token } = await socket.emitP("login", id);
  switch (code) {
    case 404: {
      const keyPub = await priRsa.exportPublic();
      const { code: codeNew } = await socket.emitP("newUser", {
        pseudo,
        id,
        idAccount,
        key: keyPub,
      });
      if (codeNew === 200) {
        await login({ pseudo });
        const devices = await getAll("device");

        const { code: codeAddDevices } = await socket.emitP(
          "addDevices",
          devices.map((device) => ({ id: device.id, key: device.key })),
        );
        if (codeAddDevices !== 200) {
          console.error(`can not addDevices error code:${codeAddDevices}`);
        }
        break;
      }
      throw new Error(`can not newUser error code:${codeNew}`);
    }
    case 200: {
      let tokenClear;
      try {
        tokenClear = await priRsa.decryptBin(token);
      } catch (e) {
        console.log(e);
        console.log("clé error");

        throw new Error(`can not auth error key`);
      }

      const {
        code: codeAuth,
        data: { connecters, messages },
      } = await socket.emitP("auth", tokenClear);
      if (codeAuth === 200) {
        workerInInterface.postMessage("login", {
          pseudo,
          id: binaryTobase64url(id),
          idAccount: binaryTobase64url(idAccount),
        });
        workerInInterface.postMessage("setFriends", await getAll("friend"));
        workerInInterface.postMessage("setDevices", await getAll("device"));
        workerInInterface.postMessage("setConnecter", connecters);
        console.log("messages", messages);
        await Promise.all(messages.map(traiteMessage));
        break;
      }
      throw new Error(`can not auth error code:${codeAuth}`);
    }
    default:
      throw new Error(`can not auth error code:${code}`);
  }
}

async function createAccount(pseudo, name, password) {
  try {
    if (!priRsa) {
      const data = await PriRsa.getRSA(password);
      priRsa = data.crypto;
      await setF("keys", data.exportKey);
    }
    id = self.crypto.getRandomValues(new Uint8Array(33)).buffer;

    const { code, data: idDistant } = await socket.emitP("newUser", {
      pseudo,
      key: priRsa.binPublicKey,
      id,
    });
    switch (code) {
      case 200: {
        await setF("pseudo", pseudo);
        await setF("id", id);
        idAccount = id;
        await setF("idAccount", idAccount);
        const data = await Aes.getAes([priRsa], [], true);
        aes = data.crypto;
        await setF("keyAes", data.exportRsaKeys[0]);
        await set(undefined, { id, key: priRsa.binPublicKey, name }, "device");
        await login({ pseudo });
        return { success: true };
      }
      case 409:
        return { success: false, idDistant, password };
      default:
        throw new Error(`can not auth error code:${code}`);
    }
  } catch (e) {
    console.error(e);
    return;
  }
}

socket.io.on("reconnect", async () => {
  console.log("reconnect");
  if (priRsa) {
    let pseudo = await get("pseudo");
    await login({ pseudo });
  }
});

socket.on("connect", async () => {
  console.log("connect");
  try {
    let pseudo = await get("pseudo");
    let idAccount = await get("idAccount");
    let id = await get("id");
    let keys = await get("keys");
    let keyAes = await get("keyAes");
    if (!idAccount || !id || !keys || !keyAes) {
      PriRsa.generateRSA();
      workerInInterface.postMessage("connected", {});
      return;
    }
    workerInInterface.postMessage("connected", { pseudo });
    if (!pseudo) {
      PriRsa.generateRSA();
    }
  } catch (e) {
    console.error(e);
  }
});

socket.on("addConnecter", (connecter) => {
  workerInInterface.postMessage("addConnecter", connecter);
});
socket.on("delConnecter", (id) => {
  workerInInterface.postMessage("delConnecter", id);
});
socket.on(
  "requestCreateDevice",
  async ({ sign, name, id: idDistant, key }, callback) => {
    try {
      // eslint-disable-next-line  no-constant-condition
      while (true) {
        workerInInterface.postMessage("requestCreateDevice");
        let { success, password } = await new Promise((resolve) => {
          workerInInterface.addEventListener(
            "requestCreateDeviceReponse",
            async ({ detail: { success, password } }) => {
              resolve({ success, password });
            },
            { once: true },
          );
        });
        if (success) {
          success = await PriRsa.testPassword(await get("keys"), password);
        } else {
          callback({ success: false, passwordWrong: false });
          break;
        }

        if (success) {
          const pubKey = await PubRsa.import(key);

          if (await pubKey.verifyText(sign, password)) {
            rsaKeysList[idDistant] = pubKey;
            const signPassword = await priRsa.signText(password);

            const keyAes = await Aes.export(
              await get("keyAes"),
              priRsa,
              rsaKeysList[idDistant],
            );
            const signAes = await priRsa.signBin(keyAes);
            console.log(signAes, signPassword);
            callback({
              success: true,
              idAccount,
              key: keyAes,
              signPassword,
              signAes,
            });
            const device = {
              id: idDistant,
              key: pubKey.binPublicKey,
              name,
            };
            await set(undefined, device, "device");
            workerInInterface.postMessage("addDevice", device);
            const devices = await getAll("device");
            const friends = await getAll("friend");
            let data = await aes.encryptObject({
              data: { devices, friends },
              type: "sendDBs",
            });
            console.log({
              destination: [idDistant],
              data: { data, type: "internal" },
              persist: true,
            });
            socket.emit("sendMessage", {
              destination: [idDistant],
              data: { data, type: "internal" },
              persist: true,
            });
            const destination = devices
              .filter(
                (device) =>
                  !arraybufferEqual(device.id, id) &&
                  !arraybufferEqual(device.id, idDistant),
              )
              .map((device) => device.id);
            console.log(destination, binaryTobase64url(id), idDistant);
            if (destination.length) {
              data = await aes.encryptObject({
                data: { device },
                type: "addDevice",
              });
              socket.emit("sendMessage", {
                destination,
                data: { data, type: "internal" },
                persist: true,
              });
            }
          } else {
            callback({ success: false, passwordWrong: true });
          }
          break;
        } else {
          workerInInterface.postMessage("toast", "Mot de passe incorrect");
        }
      }
    } catch (e) {
      console.error(e);
    }
  },
);

workerInInterface.addEventListener(
  "requestCreateDevice",
  async ({ detail: { pseudo, name, idDistant, password } }) => {
    try {
      const rsaPubKey = await getRsaKey(idDistant);

      const sign = await priRsa.signText(password);

      const {
        code,
        data: {
          passwordWrong,
          key,
          signPassword,
          signAes,
          idAccount: idAccountResponse,
        },
      } = await socket.emitP("requestCreateDevice", {
        pseudo,
        key: priRsa.binPublicKey,
        id,
        idDistant,
        data: {
          sign,
          name,
        },
      });
      let verify = true;
      if (code === 200) {
        verify =
          (await rsaPubKey.verifyBin(signAes, key)) &&
          (await rsaPubKey.verifyText(signPassword, password));
        if (verify) {
          await setF("keyAes", key);
          aes = await Aes.importRsa(key, priRsa, true);
          await setF("pseudo", pseudo);
          console.log(id, key);
          await setF("id", id);
          await setF("idAccount", idAccountResponse);
          idAccount = idAccountResponse;
          await login({ pseudo });
        }
      }
      if (!(code === 200 && verify)) {
        PriRsa.generateRSA();
        priRsa = undefined;
        id = undefined;
        idAccount = undefined;
      }
      workerInInterface.postMessage("requestCreateDeviceReponse", {
        success: code === 200 && verify,
        passwordWrong: passwordWrong || !verify,
      });
    } catch (error) {
      console.error(error);
    }
  },
);

workerInInterface.addEventListener(
  "addFriend",
  async ({ detail: { destinations } }) => {
    try {
      idRequestFriend = destinations;
      await Promise.all(
        destinations.map(async (id) => {
          const key = await getRsaKey(id);

          const dataEncrypt = await key.encryptObject({
            type: "addFriendRequest",
          });

          socket.emit("sendMessage", {
            destination: [id],
            data: { data: dataEncrypt, type: "message" },
            persist: false,
          });
        }),
      );
    } catch (e) {
      console.error(e);
    }
  },
);

workerInInterface.addEventListener(
  "createAccount",
  async ({ detail: { pseudo, name, password } }) => {
    try {
      const data = await createAccount(pseudo, name, password);
      workerInInterface.postMessage("createAccount", { pseudo, ...data });
    } catch (e) {
      workerInInterface.postMessage("createAccount", {
        pseudo,
        success: false,
      });
    }
  },
);

workerInInterface.addEventListener(
  "login",
  async ({ detail: { password } }) => {
    try {
      priRsa = await PriRsa.import(await get("keys"), password);
      const keyAes = await get("keyAes");
      if (keyAes) {
        aes = await Aes.importRsa(keyAes, priRsa, true);
      } else {
        const data = await Aes.getAes([priRsa], [], true);
        aes = data.crypto;
        await set("keyAes", data.exportRsaKeys[0]);
      }
    } catch (e) {
      console.error(e);
      workerInInterface.postMessage("toast", "Mot de passe incorrect");
      return;
    }
    id = await get("id");
    idAccount = await get("idAccount");
    let pseudo = await get("pseudo");
    try {
      await login({ pseudo });
    } catch (error) {
      console.error(error);
      await cleanAll();
      priRsa = undefined;
      id = undefined;
      idAccount = undefined;
      workerInInterface.postMessage("goToCreateAccount");
      return;
    }
  },
);
workerInInterface.addEventListener("putDevice", async ({ detail: device }) => {
  await put(device.id, device, "device");
  const destination = (await getAll("device")).map((device) => device.id);

  const data = await aes.encryptObject({
    data: { device },
    type: "putDevice",
  });
  socket.emit("sendMessage", {
    destination,
    data: { data, type: "internal" },
    persist: true,
  });
});
workerInInterface.addEventListener("delDevice", async ({ detail: { id } }) => {
  const destination = (await getAll("device")).map((device) => device.id);

  await del(id, "device");

  const data = await aes.encryptObject({
    data: { id },
    type: "delDevice",
  });
  socket.emit("sendMessage", {
    destination,
    data: { data, type: "internal" },
    persist: true,
  });
  const { code: codeDelDevice } = await socket.emitP("delDevice", id);
  if (codeDelDevice !== 200) {
    console.error(`can not delDevice error code:${codeDelDevice}`);
  }
});

workerInInterface.addEventListener("clean", async () => {
  try {
    const destination = (await getAll("device")).map((device) => device.id);
    const data = await aes.encryptObject({
      data: { id },
      type: "delDevice",
    });
    socket.emit("sendMessage", {
      destination,
      data: { data, type: "internal" },
      persist: true,
    });
    const { code: codeDelDevice } = await socket.emitP("delDevice", id);
    if (codeDelDevice !== 200) {
      console.error(`can not delDevice error code:${codeDelDevice}`);
    }

    await cleanAll();

    workerInInterface.postMessage("goToCreateAccount");
    socket.emit("logout");
    priRsa = undefined;
    aes = undefined;
    id = undefined;
  } catch (e) {
    console.error(e);
  }
});
workerInInterface.addEventListener("logout", async () => {
  socket.emit("logout");
  priRsa = undefined;
  aes = undefined;
  id = undefined;
  idAccount = undefined;
});

workerInInterface.addEventListener(
  "sendMessage",
  async ({ detail: { destinations, channelType, ...data } }) => {
    console.log({ destinations, channelType, ...data });
    try {
      switch (channelType) {
        case "general":
          socket.emit("sendMessage", {
            destination: "general",
            destinationType: "general",
            data: { ...data, type: "publicMessage" },
          });
          break;
        case "internal": {
          const destinationsInternal = (await getAll("device")).map(
            (device) => device.id,
          );
          if (destinationsInternal.length === 1) {
            break;
          }
          data = await aes.encryptObject({
            data,
            type: "internalMessage",
          });
          socket.emit("sendMessage", {
            destination: destinationsInternal,
            destinationType: "device",
            data: { data, type: "internal" },
            persist: true,
          });
          break;
        }
        case "unknown": {
          const destinationsInternal = (await getAll("device")).map(
            (device) => device.id,
          );

          await Promise.all(
            destinations.map(async (id) => {
              console.log(id);
              const key = await getRsaKey(id);
              const dataEncrypt = await key.encryptObject({
                data,
                type: "message",
              });
              socket.emit("sendMessage", {
                destinationType: "device",
                destination: [id],
                data: { data: dataEncrypt, type: "message" },
                persist: false,
              });
            }),
          );

          if (destinationsInternal.length !== 1) {
            data = await aes.encryptObject({
              data,
              type: "messageMP",
            });
            socket.emit("sendMessage", {
              destinationType: "device",
              destination: destinationsInternal,
              data: { data, type: "internal" },
              persist: false,
            });
          }
          break;
        }
        case "friend": {
          const aesFriend = await getAesKey(data.idChannel);
          const dataEncrypt = await aesFriend.encryptObject({
            data,
            type: "message",
          });
          socket.emit("sendMessage", {
            destinationType: "account",
            destination: data.idChannel,
            data: { data: dataEncrypt, type: "friend" },
            persist: true,
          });
          const destinationsInternal = (await getAll("device")).map(
            (device) => device.id,
          );
          console.log("destinationsInternal", destinationsInternal);
          if (destinationsInternal.length !== 1) {
            data = await aes.encryptObject({
              data,
              type: "messageMPFriend",
            });
            socket.emit("sendMessage", {
              destinationType: "device",
              destination: destinationsInternal,
              data: { data, type: "internal" },
              persist: true,
            });
          }
          break;
        }
      }
    } catch (error) {
      console.error(error);
    }
  },
);

socket.on("message", traiteMessage);
