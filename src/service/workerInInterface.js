const myEventTarget = new EventTarget();

self.addEventListener("message", function (e) {
  let event = new CustomEvent(e.data.event, { detail: e.data.data });

  myEventTarget.dispatchEvent(event);
});

myEventTarget.postMessage = (event, data) => {
  self.postMessage({ event, data });
};

export default myEventTarget;
