const dbP = new Promise((resolve, reject) => {
  const request = self.indexedDB.open("epsila", 1);
  request.onerror = reject;
  request.onsuccess = function (e) {
    request.result.onerror = function (event) {
      console.error(event);
    };
    resolve(e.target.result);
  };
  request.onupgradeneeded = function (event) {
    var db = event.target.result;

    db.onerror = function (event) {
      console.error(event);
    };
    switch (event.oldVersion) {
      case 0:
        db.createObjectStore("kv", {});
        db.createObjectStore("friend", { keyPath: "id" });
        db.createObjectStore("device", { keyPath: "id" });
        break;
    }
  };
});
const reqToPromise = async (objectStoreRequest) => {
  return new Promise((resolve, reject) => {
    objectStoreRequest.onsuccess = (event) => {
      resolve(event.target.result);
    };
    objectStoreRequest.onerror = (error) => {
      reject(error);
    };
  });
};
export const get = async (key, table = "kv") => {
  const db = await dbP;

  const transaction = db.transaction(table, "readonly");
  const objectStore = transaction.objectStore(table);
  return reqToPromise(objectStore.get(key));
};

export const getAll = async (table = "kv") => {
  const db = await dbP;
  const transaction = db.transaction(table, "readonly");
  const objectStore = transaction.objectStore(table);
  return reqToPromise(objectStore.getAll());
};

export const set = async (key, value, table = "kv") => {
  const db = await dbP;
  const transaction = db.transaction(table, "readwrite");
  const objectStore = transaction.objectStore(table);
  await reqToPromise(objectStore.add(value, key));
};

export const setF = async (key, value, table = "kv") => {
  const db = await dbP;
  const transaction = db.transaction(table, "readwrite");
  const objectStore = transaction.objectStore(table);
  await reqToPromise(objectStore.put(value, key));
};
export const put = async (key, value, table) => {
  const db = await dbP;
  const transaction = db.transaction(table, "readwrite");
  const objectStore = transaction.objectStore(table);

  const oldObjet = await reqToPromise(objectStore.get(key));
  await reqToPromise(
    objectStore.put({
      ...oldObjet,
      ...value,
    }),
  );
};
export const del = async (key, table = "kv") => {
  const db = await dbP;
  const transaction = db.transaction(table, "readwrite");
  const objectStore = transaction.objectStore(table);
  await reqToPromise(objectStore.delete(key));
};
export const setMultiple = async (values, table) => {
  const db = await dbP;
  const transaction = db.transaction(table, "readwrite");
  const objectStore = transaction.objectStore(table);
  Promise.all(
    values.map(async (value) => reqToPromise(objectStore.add(value))),
  );
};
export const cleanAll = async () => {
  const db = await dbP;

  const transaction = db.transaction(db.objectStoreNames, "readwrite");

  await Promise.all(
    Array.from(db.objectStoreNames).map(async (storeName) => {
      const objectStore = transaction.objectStore(storeName);
      await reqToPromise(objectStore.clear());
    }),
  );
};
export const clean = async (table) => {
  const db = await dbP;

  const transaction = db.transaction(table, "readwrite");

  const objectStore = transaction.objectStore(table);
  await reqToPromise(objectStore.clear());
};
