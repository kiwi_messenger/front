const constante = {};

const addConst = (name, value) => {
  Object.freeze(value);
  Object.defineProperty(constante, name, {
    value,
    writable: false,
  });
};

// constante
addConst("algoForEncript", { name: "RSA-OAEP", hash: "SHA-512" });
addConst("algoForAes", { name: "AES-GCM", length: 256 });
addConst("algoForSign", {
  name: "RSA-PSS",
  hash: "SHA-512",
  saltLength: 64,
});
addConst("keyUsagesForEncript", {
  private: ["decrypt", "unwrapKey"],
  public: ["encrypt", "wrapKey"],
  full: ["decrypt", "unwrapKey", "encrypt", "wrapKey"],
  aes: ["decrypt", "encrypt"],
});
addConst("keyUsagesForSign", {
  private: ["sign"],
  public: ["verify"],
  full: ["verify", "sign"],
});
addConst("formatBin", { public: "spki", private: "pkcs8", aes: "raw" });

Object.freeze(constante);

export default constante;
