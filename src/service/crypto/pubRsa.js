import { textToBinary } from "@/service/function";
import constante from "./constante.js";
import notepack from "notepack.io";

// constructeur
const PubRsa = function () {
  this.keyForEncript = {};
  this.keyForSign = {};
  this.binPublicKey = undefined;
};

PubRsa.import = async function (publicKey) {
  const pubRsa = new this();
  pubRsa.binPublicKey = publicKey;

  pubRsa.keyForEncript.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    publicKey,
    constante.algoForEncript,
    false,
    constante.keyUsagesForEncript.public,
  );
  pubRsa.keyForSign.publicKey = await self.crypto.subtle.importKey(
    constante.formatBin.public,
    publicKey,
    constante.algoForSign,
    false,
    constante.keyUsagesForSign.public,
  );
  return pubRsa;
};

PubRsa.imports = async function (publicKeys) {
  return Promise.all(publicKeys.map(PubRsa.import.bind(PubRsa)));
};

// methode
PubRsa.prototype.verifyBin = async function (sign, bin) {
  return self.crypto.subtle.verify(
    constante.algoForSign,
    this.keyForSign.publicKey,
    sign,
    bin,
  );
};
PubRsa.prototype.verifyText = async function (sign, text) {
  return this.verifyBin(sign, await textToBinary(text));
};

PubRsa.prototype.encryptBin = async function (message) {
  try {
    return await self.crypto.subtle.encrypt(
      constante.algoForEncript,
      this.keyForEncript.publicKey,
      message,
    );
  } catch (e) {
    console.error(e);
    throw new Error("error encrypt message");
  }
};
PubRsa.prototype.encryptObject = async function (message) {
  console.log(await notepack.encode(message));
  return await this.encryptBin(await notepack.encode(message));
};

PubRsa.prototype.encryptText = async function (message) {
  return await this.encryptBin(await textToBinary(message));
};

PubRsa.prototype.exportPublic = async function () {
  return self.crypto.subtle.exportKey(
    constante.formatBin.public,
    this.keyForEncript.publicKey,
  );
};

Object.freeze(PubRsa);
Object.freeze(PubRsa.prototype);

export default PubRsa;
