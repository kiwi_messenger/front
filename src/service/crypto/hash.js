import { textToBinary } from "@/service/function";

const bin = async (bin) => {
  return self.crypto.subtle.digest("SHA-512", bin);
};

const text = async (text) => {
  return bin(await textToBinary(text));
};

export default {
  bin,
  text,
};
