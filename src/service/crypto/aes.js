import constante from "./constante";
import { binaryToText, textToBinary } from "@/service/function";
import notepack from "notepack.io";

// constructeur
const Aes = function () {
  this.key;
};

// methode static
Aes.importRsa = async function (aesKey, rsaPriKey, full) {
  const aes = new this();

  aes.key = await self.crypto.subtle.unwrapKey(
    constante.formatBin.aes,
    aesKey,
    rsaPriKey.keyForEncript.privateKey,
    constante.algoForEncript,
    constante.algoForAes,
    false,
    full
      ? constante.keyUsagesForEncript.full
      : constante.keyUsagesForEncript.aes,
  );
  return aes;
};

Aes.importAes = async function ({ data, iv }, aesKey, full) {
  const aes = new this();

  aes.key = await self.crypto.subtle.unwrapKey(
    constante.formatBin.aes,
    data,
    aesKey.key,
    { ...constante.algoForAes, iv },
    constante.algoForAes,
    false,
    full
      ? constante.keyUsagesForEncript.full
      : constante.keyUsagesForEncript.aes,
  );
  return aes;
};

Aes.export = async function (aesKey, rsaPriKey, rsaPubKey) {
  const key = await self.crypto.subtle.unwrapKey(
    constante.formatBin.aes,
    aesKey,
    rsaPriKey.keyForEncript.privateKey,
    constante.algoForEncript,
    constante.algoForAes,
    true,
    constante.keyUsagesForEncript.aes,
  );

  return await self.crypto.subtle.wrapKey(
    constante.formatBin.aes,
    key,
    rsaPubKey.keyForEncript.publicKey,
    constante.algoForEncript,
  );
};

Aes.exportRSAToAES = async function (aesBinKey, rsaPriKey, aesKey) {
  const key = await self.crypto.subtle.unwrapKey(
    constante.formatBin.aes,
    aesBinKey,
    rsaPriKey.keyForEncript.privateKey,
    constante.algoForEncript,
    constante.algoForAes,
    true,
    constante.keyUsagesForEncript.aes,
  );
  let iv = self.crypto.getRandomValues(new Uint8Array(12)).buffer;

  return {
    data: await self.crypto.subtle.wrapKey(
      constante.formatBin.aes,
      key,
      aesKey.key,
      { ...constante.algoForAes, iv },
    ),
    iv,
  };
};

Aes.exports = async function (aesKey, rsaPriKey, rsaPubKeys) {
  const key = await self.crypto.subtle.unwrapKey(
    constante.formatBin.aes,
    aesKey,
    rsaPriKey.keyForEncript.privateKey,
    constante.algoForEncript,
    constante.algoForAes,
    true,
    constante.keyUsagesForEncript.aes,
  );
  return await Promise.all(
    rsaPubKeys.map(async (rsaPubKey) =>
      self.crypto.subtle.wrapKey(
        constante.formatBin.aes,
        key,
        rsaPubKey.keyForEncript.publicKey,
        constante.algoForEncript,
      ),
    ),
  );
};

Aes.getAes = async function (rsaPubKeys, aesKeys = [], full = false) {
  console.log({ rsaPubKeys, aesKeys, full });
  const aes = new this();
  const aesKey = await self.crypto.subtle.generateKey(
    constante.algoForAes,
    true,
    constante.keyUsagesForEncript.aes,
  );
  const aesKeyBin = await self.crypto.subtle.exportKey(
    constante.formatBin.aes,
    aesKey,
  );
  const exportRsaKeys = Promise.all(
    rsaPubKeys.map(async (rsaPubKey) =>
      self.crypto.subtle.wrapKey(
        constante.formatBin.aes,
        aesKey,
        rsaPubKey.keyForEncript.publicKey,
        constante.algoForEncript,
      ),
    ),
  );
  const exportAesKeys = Promise.all(
    aesKeys.map(async (aesPubKey) => {
      let iv = self.crypto.getRandomValues(new Uint8Array(12)).buffer;
      return {
        data: await self.crypto.subtle.wrapKey(
          constante.formatBin.aes,
          aesKey,
          aesPubKey.key,
          { ...constante.algoForAes, iv },
        ),
        iv,
      };
    }),
  );

  aes.key = await self.crypto.subtle.importKey(
    constante.formatBin.aes,
    aesKeyBin,
    constante.algoForAes,
    false,
    full
      ? constante.keyUsagesForEncript.full
      : constante.keyUsagesForEncript.aes,
  );
  return {
    crypto: aes,
    exportRsaKeys: await exportRsaKeys,
    exportAesKeys: await exportAesKeys,
  };
};

// methode
// decrypt
Aes.prototype.decryptBin = async function ({ data, iv }) {
  try {
    return await self.crypto.subtle.decrypt(
      {
        ...constante.algoForAes,
        iv,
      },
      this.key,
      data,
    );
  } catch (e) {
    console.error(e);
    throw new Error("error decript message");
  }
};
Aes.prototype.decryptText = async function (message) {
  return await binaryToText(await this.decryptBin(message));
};
Aes.prototype.decryptObject = async function (message) {
  return await notepack.decode(await this.decryptBin(message));
};

// encrypt
Aes.prototype.encryptBin = async function (message) {
  try {
    let iv = self.crypto.getRandomValues(new Uint8Array(12)).buffer;

    return {
      data: await self.crypto.subtle.encrypt(
        {
          ...constante.algoForAes,
          iv,
        },
        this.key,
        message,
      ),
      iv,
    };
  } catch (e) {
    console.error(e);
    throw new Error("error encrypt message");
  }
};
Aes.prototype.encryptText = async function (message) {
  return await this.encryptBin(await textToBinary(message));
};
Aes.prototype.encryptObject = async function (message) {
  return await this.encryptBin(await notepack.encode(message));
};

export default Aes;
