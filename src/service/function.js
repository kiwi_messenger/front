export const binaryToText = (buffer) => {
  let enc = new TextDecoder();
  return enc.decode(buffer);
};

export const textToBinary = (message) => {
  const enc = new TextEncoder();
  return enc.encode(message);
};

export const binaryTobase64 = (buffer) => {
  return btoa(String.fromCharCode(...new Uint8Array(buffer)));
};

export const binaryTobase64url = (buffer) => {
  return binaryTobase64(buffer)
    .replace(/=+$/, "")
    .replace(/\+/g, "-")
    .replace(/\//g, "_");
};

export const base64ToBinary = (base64) => {
  console.log(base64);
  const binaryString = atob(base64);
  return Uint8Array.from(binaryString, (char) => char.charCodeAt(0)).buffer;
};

export const base64urlToBinary = (base64url) => {
  const base64 = base64url.replace(/-/g, "+").replace(/_/g, "/");
  console.log(base64url.length);
  return base64ToBinary(`${base64}${"=".repeat(4 - (base64.length % 4 || 4))}`);
};

export const arraybufferEqual = (buf1, buf2) => {
  if (buf1 === buf2) {
    return true;
  }

  if (buf1.byteLength !== buf2.byteLength) {
    return false;
  }

  var view1 = new DataView(buf1);
  var view2 = new DataView(buf2);

  var i = buf1.byteLength;
  while (i--) {
    if (view1.getUint8(i) !== view2.getUint8(i)) {
      return false;
    }
  }

  return true;
};
export const concat = (buf1, buf2) => {
  // Créez un nouveau ArrayBuffer de la taille combinée
  let combinedBuffer = new ArrayBuffer(buf1.byteLength + buf2.byteLength);

  // Créez un Uint8Array pour manipuler le combinedBuffer
  let combinedView = new Uint8Array(combinedBuffer);

  // Copiez les données des deux ArrayBuffer dans le nouveau
  combinedView.set(new Uint8Array(buf1), 0);
  combinedView.set(new Uint8Array(buf2), buf1.byteLength);
  return combinedBuffer;
};
