import io from "socket.io-client";
import customParser from "socket.io-msgpack-parser";

let socket;

if (import.meta.env.PROD) {
  socket = io(import.meta.env.VITE_APP_BACKEND_URL ?? "/", {
    transports: ["websocket", "polling"],
    parser: customParser,
  });
} else {
  socket = io(import.meta.env.VITE_APP_BACKEND_URL, {
    transports: ["websocket", "polling"],
  });
}
socket.emitP = async function (...args) {
  return new Promise((resolve) => {
    args[args.length] = (code, data) => {
      resolve({ code, data });
    };
    this.emit(...args);
  });
};
export default socket;
