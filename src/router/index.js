import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../views/HomePage.vue";

const routes = [
  {
    path: "/chat",
    name: "Chat",
    component: () =>
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      import("../views/ChatPage.vue"),
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/AboutPage.vue"),
  },
  {
    path: "/privacyPolicy",
    name: "PrivacyPolicy",
    component: () => import("../views/PrivacyPolicyPage.vue"),
  },
  {
    path: "/createAccount",
    name: "CreateAccount",
    component: () => import("../views/CreateAccountPage.vue"),
  },
  {
    path: "/setting",
    name: "Setting",
    component: () => import("../views/SittingPage.vue"),
  },
  {
    path: "/",
    name: "Home",
    component: HomePage,
  },
  { path: "/:pathMatch(.*)*", redirect: "/" },
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

export default router;
