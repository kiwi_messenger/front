import workerInterface from "@/service/workerInterface";
import { binaryTobase64url } from "@/service/function";

export default function createWebSocketPlugin(store) {
  store.registerModule("connects", {
    namespaced: true,
    state: {},
    mutations: {
      setConnecter(state, users) {
        Object.keys(users).reduce((accu, id) => {
          console.log("test", users[id].idAccount, store.state.idAccount);
          if (
            users[id].idAccount === store.state.idAccount ||
            store.state.friends[users[id].idAccount]
          ) {
            return accu;
          }
          if (!accu[users[id].idAccount]) {
            accu[users[id].idAccount] = {
              channelType: "unknown",
              id: users[id].idAccount,
              messages: [],
              name: users[id].pseudo,
              description:
                "Conversation temporaire avec une personne connecter",
              notif: 0,
              users: {},
              encryption: "RSA",
              canAddFriend: true,
              online: true,
            };
          }
          accu[users[id].idAccount].users[id] = users[id].pseudo;
          return accu;
        }, state);
        console.log(state);
      },
      delConnecter(state, id) {
        Object.keys(state).forEach((idAccount) => {
          delete state[idAccount].users[id];
          if (Object.keys(state[idAccount].users).length === 0) {
            delete state[idAccount];
          }
        });
      },
      addFriend(state, { id }) {
        delete state[binaryTobase64url(id)];
      },
      addConnecter(state, { pseudo, id, idAccount }) {
        console.log("store.state.pseudo", store.state.pseudo);
        if (
          idAccount === store.state.idAccount ||
          store.state.friends[idAccount]
        ) {
          return;
        }
        if (!state[idAccount]) {
          state[idAccount] = {
            channelType: "unknown",
            id: idAccount,
            messages: [],
            name: pseudo,
            description: "Conversation temporaire avec une personne connecter",
            notif: 0,
            users: {},
            encryption: "RSA",
            canAddFriend: true,
            online: true,
          };
        }
        state[idAccount].users[id] = pseudo;
      },
      clear(state) {
        Object.keys(state).reduce((stateInReduce, key) => {
          delete stateInReduce[key];
          return stateInReduce;
        }, state);
      },
      message(state, message) {
        console.log(state, message);
        state[message.idChannel].messages.push({
          idUser: message.idUser,
          message: message.message,
          correspondant: message.correspondant,
          name: message.name,
        });
        if (!state[message.idChannel].select) {
          state[message.idChannel].notif++;
        }
      },
      selectChannel(state, { idChannel }) {
        state[idChannel].select = true;
        state[idChannel].notif = 0;
      },
    },
  });
  workerInterface.addEventListener("addConnecter", (e) => {
    store.commit("connects/addConnecter", e.detail);
  });
  workerInterface.addEventListener("delConnecter", (e) => {
    store.commit("connects/delConnecter", e.detail);
  });

  workerInterface.addEventListener("setConnecter", (e) => {
    store.commit("connects/setConnecter", e.detail);
  });
  workerInterface.addEventListener("messageMP", (e) => {
    store.commit("connects/message", e.detail);
  });

  workerInterface.addEventListener("addFriend", (e) => {
    store.commit("connects/addFriend", e.detail);
  });
}
