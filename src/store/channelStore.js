import workerInterface from "@/service/workerInterface";

const defaultState = {
  general: {
    channelType: "general",
    id: "general",
    messages: [],
    name: "#Général",
    description:
      "Channel général, tout le monde peut écrire et voir les messages",
    notif: 0,
    users: {},
    encryption: "none",
    canAddFriend: false,
    online: true,
  },
  internal: {
    channelType: "internal",
    id: "internal",
    messages: [],
    name: "#Interne",
    description:
      "Channel interne a votre compte, seul vous pouvez voir les messages ici",
    notif: 0,
    users: {},
    encryption: "AES",
    canAddFriend: false,
    online: true,
  },
};

export default function createWebSocketPlugin(store) {
  store.registerModule("channels", {
    namespaced: true,
    state: defaultState,
    mutations: {
      setConnecter(state, users) {
        state.general.users = users;
      },
      delConnecter(state, id) {
        Object.keys(state).forEach((pseudo) => {
          delete state[pseudo].users[id];
        });
      },
      addConnecter(state, { pseudo, id }) {
        state.general.users[id] = pseudo;
      },
      clear(state) {
        Object.keys(state).reduce((stateInReduce, key) => {
          if (!defaultState[key]) {
            delete stateInReduce[key];
          }
          return stateInReduce;
        }, state);
        Object.assign(state, defaultState);
      },
      message(state, message) {
        console.log(state, message);
        state[message.idChannel].messages.push({
          idUser: message.idUser,
          message: message.message,
          correspondant: message.correspondant,
          name: message.name,
        });
        if (!state[message.idChannel].select) {
          state[message.idChannel].notif++;
        }
      },
      selectChannel(state, { idChannel }) {
        state[idChannel].select = true;
        state[idChannel].notif = 0;
      },
    },
  });
  workerInterface.addEventListener("addConnecter", (e) => {
    store.commit("channels/addConnecter", e.detail);
  });
  workerInterface.addEventListener("delConnecter", (e) => {
    store.commit("channels/delConnecter", e.detail);
  });

  workerInterface.addEventListener("setConnecter", (e) => {
    store.commit("channels/setConnecter", e.detail);
  });
  workerInterface.addEventListener("message", (e) => {
    store.commit("channels/message", e.detail);
  });
}
