import workerInterface from "@/service/workerInterface";
import { binaryTobase64url } from "@/service/function";

export default function createWebSocketPlugin(store) {
  store.registerModule("devices", {
    namespaced: true,
    state: {},
    mutations: {
      setDevices(state, devices) {
        devices.reduce((accu, device) => {
          accu[binaryTobase64url(device.id)] = device;
          return accu;
        }, state);
        console.log(state);
      },
      addDevice(state, device) {
        state[binaryTobase64url(device.id)] = device;
      },
      putDevice(state, device) {
        state[binaryTobase64url(device.id)] = {
          ...state[binaryTobase64url(device.id)],
          ...device,
        };
      },
      delDevice(state, { id }) {
        delete state[binaryTobase64url(id)];
      },
      clear(state) {
        Object.keys(state).reduce((stateInReduce, key) => {
          delete stateInReduce[key];
          return stateInReduce;
        }, state);
      },
      setConnecter(state, users) {
        Object.keys(users).reduce((accu, id) => {
          if (accu[id]) {
            accu[id].isConnect = true;
          }
          return accu;
        }, state);
      },
      addConnecter(state, { id }) {
        if (state[id]) {
          state[id].isConnect = true;
        }
      },
      delConnecter(state, id) {
        if (state[id]) {
          state[id].isConnect = false;
        }
      },
    },
  });
  workerInterface.addEventListener("addDevice", (e) => {
    store.commit("devices/addDevice", e.detail);
  });
  workerInterface.addEventListener("putDevice", (e) => {
    store.commit("devices/putDevice", e.detail);
  });
  workerInterface.addEventListener("setDevices", (e) => {
    store.commit("devices/setDevices", e.detail);
  });
  workerInterface.addEventListener("delDevice", (e) => {
    store.commit("devices/delDevice", e.detail);
  });
  workerInterface.addEventListener("addConnecter", (e) => {
    store.commit("devices/addConnecter", e.detail);
  });
  workerInterface.addEventListener("delConnecter", (e) => {
    store.commit("devices/delConnecter", e.detail);
  });
  workerInterface.addEventListener("setConnecter", (e) => {
    store.commit("devices/setConnecter", e.detail);
  });
}
