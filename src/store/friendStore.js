import workerInterface from "@/service/workerInterface";
import { binaryTobase64url } from "@/service/function";

export default function createWebSocketPlugin(store) {
  store.registerModule("friends", {
    namespaced: true,
    state: {},
    mutations: {
      setFriends(state, users) {
        console.log("setFriends", users);
        users.reduce((accu, user) => {
          user.id = binaryTobase64url(user.id);
          if (!accu[user.id]) {
            accu[user.id] = {
              channelType: "friend",
              id: user.id,
              messages: [],
              name: user.pseudo,
              description: "Conversation avec un ami",
              notif: 0,
              users: {},
              encryption: "AES",
              canAddFriend: false,
              online: false,
            };
          }
          return accu;
        }, state);
        console.log(state);
      },
      addConnecter(state, { pseudo, id, idAccount }) {
        console.log({ pseudo, id, idAccount });
        console.log(state);
        if (state[idAccount]) {
          state[idAccount].online = true;
          state[idAccount].users[id] = pseudo;
        }
      },
      delConnecter(state, id) {
        Object.keys(state).forEach((idAccount) => {
          delete state[idAccount].users[id];
          if (Object.keys(state[idAccount].users).length === 0) {
            state[idAccount].online = false;
          }
        });
      },
      setConnecter(state, users) {
        Object.keys(users).reduce((accu, id) => {
          const idAccount = users[id].idAccount;
          if (accu[idAccount]) {
            accu[idAccount].online = true;
            accu[idAccount].users[id] = users[id].pseudo;
          }
          return accu;
        }, state);
      },
      delFriend(state, id) {
        Object.keys(state).forEach((idAccount) => {
          delete state[idAccount].users[id];
          if (Object.keys(state[idAccount].users).length === 0) {
            delete state[idAccount];
          }
        });
      },
      addFriend(state, { pseudo, id }) {
        id = binaryTobase64url(id);
        if (!state[id]) {
          state[id] = {
            channelType: "friend",
            id,
            messages: [],
            name: pseudo,
            description: "Conversation avec un ami",
            notif: 0,
            users: {},
            encryption: "AES",
            canAddFriend: false,
            online: true,
          };
        }
      },
      clear(state) {
        Object.keys(state).reduce((stateInReduce, key) => {
          delete stateInReduce[key];
          return stateInReduce;
        }, state);
        console.log("clear", state);
      },
      message(state, message) {
        console.log("friends", state, message);
        state[message.idChannel].messages.push({
          idUser: message.idUser,
          message: message.message,
          correspondant: message.correspondant,
          name: message.name,
        });
        if (!state[message.idChannel].select) {
          state[message.idChannel].notif++;
        }
      },
      selectChannel(state, { idChannel }) {
        state[idChannel].select = true;
        state[idChannel].notif = 0;
      },
    },
  });
  workerInterface.addEventListener("addConnecter", (e) => {
    store.commit("friends/addConnecter", e.detail);
  });
  workerInterface.addEventListener("delConnecter", (e) => {
    store.commit("friends/delConnecter", e.detail);
  });
  workerInterface.addEventListener("setConnecter", (e) => {
    store.commit("friends/setConnecter", e.detail);
  });

  workerInterface.addEventListener("addFriend", (e) => {
    store.commit("friends/addFriend", e.detail);
  });
  workerInterface.addEventListener("delFriend", (e) => {
    store.commit("friends/delFriend", e.detail);
  });
  workerInterface.addEventListener("setFriends", (e) => {
    store.commit("friends/setFriends", e.detail);
  });
  workerInterface.addEventListener("messageMPFriend", (e) => {
    store.commit("friends/message", e.detail);
  });
}
