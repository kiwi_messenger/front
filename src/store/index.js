import { createStore } from "vuex";
import channelStore from "./channelStore";
import connectStore from "./connectStore";
import deviceStore from "./deviceStore";
import friendStore from "./friendStore";
import workerInterface from "@/service/workerInterface";
import router from "@/router";

const store = createStore({
  state: {
    login: false,
    pseudo: "",
    id: "",
    idAccount: "",
  },
  getters: {},
  mutations: {
    setPseudo(state, pseudo) {
      state.pseudo = pseudo;
    },
    login(state, user) {
      state.login = true;
      state.id = user.id;
      state.pseudo = user.pseudo;
      state.idAccount = user.idAccount;
    },
    logout(state) {
      state.login = false;
      store.commit("connects/clear");
      store.commit("channels/clear");
      store.commit("devices/clear");
      store.commit("friends/clear");
    },
    clean(state) {
      state.login = false;
      state.pseudo = "";
      store.commit("connects/clear");
      store.commit("channels/clear");
      store.commit("devices/clear");
      store.commit("friends/clear");
    },
    selectChannel(state, { channel }) {
      Object.keys(state.connects).forEach((key) => {
        state.connects[key].select = false;
      });
      Object.keys(state.channels).forEach((key) => {
        state.channels[key].select = false;
      });
      Object.keys(state.friends).forEach((key) => {
        state.friends[key].select = false;
      });
      switch (channel.channelType) {
        case "internal":
        case "general":
          store.commit("channels/selectChannel", {
            idChannel: channel.id,
          });
          break;
        case "unknown":
          store.commit("connects/selectChannel", {
            idChannel: channel.id,
          });
          break;
        case "friend": {
          store.commit("friends/selectChannel", {
            idChannel: channel.id,
          });
        }
      }
    },
  },
  actions: {},
  modules: {},
  plugins: [friendStore, channelStore, connectStore, deviceStore],
});
workerInterface.addEventListener("connected", (e) => {
  if (e.detail.pseudo) {
    store.commit("setPseudo", e.detail.pseudo);
    router.replace({ name: "Chat" });
  } else {
    router.replace({ name: "CreateAccount" });
  }
});
workerInterface.addEventListener("goToCreateAccount", () => {
  store.commit("clean");

  router.replace({ name: "CreateAccount" });
});
workerInterface.addEventListener("login", (e) => {
  store.commit("login", e.detail);
});
export default store;
