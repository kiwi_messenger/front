#!/bin/bash

cd "$(dirname "$0")/../../"

npm i

sed -i '/android:usesCleartextTraffic="true"/d' android/app/src/main/AndroidManifest.xml

if [[ "$CI_COMMIT_REF_NAME" == "main" ]]
then
  VITE_APP_BACKEND_URL=https://epsila.tk npm run build
else
  VITE_APP_BACKEND_URL=https://dev.epsila.tk npm run build
fi

sdkmanager --install "build-tools;34.0.0" "platforms;android-34"

export PATH=./node_modules/.bin:$ANDROID_HOME/build-tools/34.0.0/:$PATH

echo "y" | sdkmanager --licenses

cap sync

cap build android \
  --androidreleasetype=AAB \
  --keystorepath=../.secure_files/epsila.key \
  --keystorepass=`jq -r '.android.release.storePassword' .secure_files/build.json` \
  --keystorealias=`jq -r '.android.release.alias' .secure_files/build.json` \
  --keystorealiaspass=`jq -r '.android.release.password' .secure_files/build.json`

cap build android \
  --androidreleasetype=APK \
  --signing-type=apksigner \
  --keystorepath=../.secure_files/epsila.key \
  --keystorepass=`jq -r '.android.release.storePassword' .secure_files/build.json` \
  --keystorealias=`jq -r '.android.release.alias' .secure_files/build.json` \
  --keystorealiaspass=`jq -r '.android.release.password' .secure_files/build.json`

